--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2 (Debian 13.2-1.pgdg100+1)
-- Dumped by pg_dump version 13.0 (Ubuntu 13.0-1.pgdg16.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: networks; Type: TABLE; Schema: public; Owner: widget
--

CREATE TABLE public.networks (
    prefix cidr NOT NULL
);


ALTER TABLE public.networks OWNER TO widget;

--
-- Name: networks networks_prefix_key; Type: CONSTRAINT; Schema: public; Owner: widget
--

ALTER TABLE ONLY public.networks
    ADD CONSTRAINT networks_prefix_key UNIQUE (prefix);


--
-- Name: TABLE networks; Type: ACL; Schema: public; Owner: widget
--

GRANT ALL ON TABLE public.networks TO widget_admin;


--
-- PostgreSQL database dump complete
--
